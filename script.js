const pattern = 'Zr|Zn|Yb|Y|Xe|W|V|U|Ts|Tm|Tl|Ti|Th|Te|Tc|Tb|Ta|Sr|Sn|Sm|Si|Sg|Se|Sc|Sb|S|Ru|Rn|Rh|Rg|Rf|Re|Rb|Ra|Pu|Pt|Pr|Po|Pm|Pd|Pb|Pa|P|Os|Og|O|Np|No|Ni|Nh|Ne|Nd|Nb|Na|N|Mt|Mo|Mn|Mg|Md|Mc|Lv|Lu|Lr|Li|La|Kr|K|Ir|In|I|Hs|Ho|Hg|Hf|He|H|Ge|Gd|Ga|Fr|Fm|Fl|Fe|F|Eu|Es|Er|Dy|Ds|Db|Cu|Cs|Cr|Co|Cn|Cm|Cl|Cf|Ce|Cd|Ca|C|Br|Bk|Bi|Bh|Be|Ba|B|Au|At|As|Ar|Am|Al|Ag|Ac',
form = document.getElementById('wordForm')
let word = document.getElementById('word').value.normalize("NFD").replace(/[\u0300-\u036f]/g, "").toLowerCase(),
regexGlobal = RegExp(`^(${pattern})*$`,'gi'),
json = '',
result = []

fetch('./final.json')
.then(response => {
	return response.json()
})
.then(data => {
	json = data
	
	process()
})
.catch(err => {
	console.error(err)
})

const process = _ => {
	let main = document.getElementById('main')
	main.innerHTML = ''
	
	if(word.match(regexGlobal)) {
		const split = w => {
			if(w.length > 0) {
				if (json[w.slice(-2)]) {
					split(w.slice(0,-2))
					result.push(w.slice(-2))
				}
				else if(json[w.slice(-1)]) {
					split(w.slice(0,-1))
					result.push(w.slice(-1))
				}
			}
		},
		createElements = (datas) => {
			let div = document.createElement('div'),
			number = document.createElement('div'),
			mass = document.createElement('div'),
			accro = document.createElement('div'),
			name = document.createElement('div')
			
			div.classList.add('element')
			number.classList.add('number')
			mass.classList.add('mass')
			accro.classList.add('accro')
			name.classList.add('name')
			
			number.innerHTML = datas.number
			mass.innerHTML = Math.round(datas.mass*1000)/1000
			name.innerHTML = datas.name
			accro.innerHTML = datas.symbol
			
			div.appendChild(number)
			div.appendChild(name)
			div.appendChild(accro)
			div.appendChild(mass)
			
			main.appendChild(div)
		}
		
		split(word)
		result.forEach(r => {
			createElements(json[r])
		})
	}
	else {
		main.innerHTML = `Word "${word}" not periodicable !`
	}
}

form.addEventListener('submit', e => {
	e.preventDefault()
	
	word = document.getElementById('word').value.normalize("NFD").replace(/[\u0300-\u036f]/g, "").toLowerCase()
	result = []
	
	process()
})
